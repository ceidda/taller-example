create database taller;

-- auto-generated definition
create table users
(
    name varchar(255) not null
        constraint users_pk
            primary key
);

alter table users
    owner to postgres;

