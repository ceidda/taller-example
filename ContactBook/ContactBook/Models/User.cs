﻿using System;
namespace ContactBook.Models
{
    public class User
    {
        private long id;
        private String name;
        private String phone;
        
        public User()
        {
        }

        public long Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Phone { get => phone; set => phone = value; }
    }
}
