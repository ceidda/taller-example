﻿using System;
using System.Collections.Generic;
using ContactBook.Models;
using RestSharp;

namespace ContactBook
{
    public class ContactService
    {
        private RestClient client;

        public ContactService()
        {
            this.client = new RestClient("http://localhost:8080");
        }

        public List<User> fetchContacts()
        {
            var request = new RestRequest("users", Method.GET);
            var response = this.client.Execute<List<User>>(request);
            return response.Data;
        }
    }
}
