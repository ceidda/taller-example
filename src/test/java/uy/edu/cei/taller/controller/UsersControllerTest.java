package uy.edu.cei.taller.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import uy.edu.cei.taller.bean.UserBean;
import uy.edu.cei.taller.dao.UserMapper;

public class UsersControllerTest {

	@Test
	public void test() {
		UserMapper userMapper = new UserMapper() {
			@Override
			public List<UserBean> all() {
				return Arrays.asList(new UserBean());
			}
			@Override
			public UserBean findByName(String name) {
				return null;
			}
			@Override
			public Long save(UserBean userBean) {
				return null;
			}
			@Override
			public UserBean fetchUserWithAddresses(Long id) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		UsersController usersController = 
				new UsersController(userMapper);
		
		List<UserBean> users = usersController.index();
		
		assertEquals(1, users.size());
	}

}
