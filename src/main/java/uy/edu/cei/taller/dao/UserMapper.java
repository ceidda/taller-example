package uy.edu.cei.taller.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import uy.edu.cei.taller.bean.UserBean;

@Mapper
public interface UserMapper {

	public List<UserBean> all();
	
	public UserBean findByName(@Param("name") String name);

	public Long save(@Param("user") UserBean user);
	
	public UserBean fetchUserWithAddresses(@Param("userId") Long id);
}
