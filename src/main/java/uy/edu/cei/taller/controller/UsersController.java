package uy.edu.cei.taller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uy.edu.cei.taller.bean.UserBean;
import uy.edu.cei.taller.dao.UserMapper;

@RestController
@RequestMapping("/users")
public class UsersController {

	private final UserMapper userMapper;

	@Autowired
	public UsersController(final UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	@GetMapping
	public List<UserBean> index() {
		return this.userMapper.all();
	}

	@GetMapping("{id}")
	public UserBean show(@PathVariable("id") final Long id) {
		return this.userMapper.fetchUserWithAddresses(id);
	}

	@GetMapping("findByName") // ?nameStartWith={name}
	public List<UserBean> fetchUserWithNameStart(
			@RequestParam(value = "nameStartWith", required = true) final String name) {
		return null;
	}

	// http://localhost:8080/users/123
	// @GetMapping("{name}")
	// public UserBean show(@PathVariable("name") final String name) {
	// return this.userMapper.findByName(name);
	// }

	@PostMapping
	public Long save(@RequestBody UserBean userBean) {
		this.userMapper.save(userBean);
		return userBean.getId();
	}

}
