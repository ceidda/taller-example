create table users
(
	id bigint not null
		constraint table_name_pk
			primary key,
	name varchar(255) not null,
	phone varchar(255) not null
);

create table addresses
(
	id bigint not null
		constraint addresses_pk
			primary key,
	addressline1 varchar(255) not null,
	user_id bigint not null
		constraint addresses__users_fk
			references users
);

create sequence users_sequence;
